import os
import moviepy.editor as mp
from moviepy.editor import *
from pydub import AudioSegment
from pydub import AudioSegment, effects  
from pathlib import Path
from moviepy.video.compositing.concatenate import concatenate_videoclips
import proglog

def addmov(typevideo):
        pathgfx =""
        if typevideo=='Instrucciones':
            pathgfx = "videos/instrucciones.mov"
        elif typevideo=='Esperado':
            pathgfx = "videos/loesperado.mov"
        elif typevideo=='Python':
            pathgfx = "videos/python.mov"
        elif typevideo=='Competencias':
            pathgfx = "videos/competencias.mov"
        else :
            pathgfx = "videos/outro.mov"
        return pathgfx
def videosedit(pathvideo, pathaudio,typevideo,cutbegin=0,cutend=0,musicnumber=1,pathout=None,level_music=50):
    if cutbegin!=0 or cutend!=0:
        os.system("mkdir tmp")
        filename = Path(pathvideo).name
        os.system("ffmpeg -i "+str(pathvideo)+" -ss "+str(cutend)+" -i "+pathvideo+"  -c copy -map 1:0 -map 0 -shortest -f nut - | ffmpeg -f nut -i - -map 0 -map -0:0 -c copy tmp/"+filename+"")
        #!ffmpeg -i $videopath -ss 5 -i $videopath  -c copy -map 1:0 -map 0 -shortest -f nut - | ffmpeg -f nut -i - -map 0 -map -0:0 -c copy $videopath

        os.system("ffmpeg -y  -i tmp/"+filename+" -ss "+str(cutbegin)+" -vcodec copy -acodec copy tmp/final"+filename+"")
        os.remove("tmp/"+filename)  # remove the tmp file])
        os.rename("tmp/final"+filename+"", "tmp/"+filename+"")
        pathvideo = "tmp/"+filename
    else:
        pathvideo = Path(pathvideo)
        os.system("mkdir tmp")
        os.system("cp "+str(pathvideo)+" tmp/")


    os.system("cp "+str(pathaudio)+" tmp/")
    pathaudio = Path(pathaudio)
    pathvideo = Path(pathvideo)
     

    pathaudio = "tmp/"+str(pathaudio.name)
    from pydub import AudioSegment

 
    rawsound = AudioSegment.from_file(pathaudio, "mp3")  
    normalizedsound = effects.normalize(rawsound)  
    normalizedsound.export(pathaudio, format="mp3")    

    backgroundmusic = "music/"+str(musicnumber)+'.mp3' #@param ["1.mp3","2.mp3", "3.mp3","4.mp3","5.mp3","6.mp3","7.mp3"]
    mindb = level_music
    from pydub import AudioSegment
    sound1 = AudioSegment.from_file(str(pathaudio), format="mp3")
    sound2 = AudioSegment.from_file(str(backgroundmusic), format="mp3")

    overlay = sound1.overlay(sound2 - int(mindb)  , position=0)
    overlay.export(pathaudio, format="mp3")
    pathaudio = Path(pathaudio)
    os.system("ffmpeg -i  tmp/"+pathvideo.name+" -i tmp/"+pathaudio.name+" -map 0:v -map 1:a -c:v copy -shortest tmp/final"+pathvideo.name+"")
    os.rename("tmp/final"+pathvideo.name+"", "tmp/"+pathvideo.name)
    video_clip = VideoFileClip(("tmp/"+pathvideo.name), target_resolution=(1080, 1920))
    overlay_clip = VideoFileClip((addmov(typevideo)), has_mask=True, target_resolution=(1080, 1920)) #.mov file with alpha channel
    outro_clip = VideoFileClip(("videos/outro.mp4"), target_resolution=(1080, 1920))
    

    final_video = mp.CompositeVideoClip([video_clip, overlay_clip])  
    final_video = concatenate_videoclips([final_video, outro_clip])
    #imprimir una barra de progreso para que el usuario sepa que esta haciendo el video
    final_video.write_videofile(
    f"{pathout}",
    fps=24,
    temp_audiofile=f"temp-audio.m4a",
    remove_temp=True,
    codec="libx264",
    audio_codec="aac",
    threads = 120,)
    os.system("rm -r tmp")

