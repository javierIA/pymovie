import tkinter as tk
from tkinter import Label, Tk,Entry,Checkbutton,Button,ttk
from tkinter import filedialog as fd
from tkinter.constants import TRUE
from tkinter.font import families
from tkinter.messagebox import showinfo
from videosedit import videosedit

# create the root window

def select_video():
    filetypes = (
        ('video files', '*.mp4'),
        ('All files', '*.*')
    )

    filename = fd.askopenfilename(
        title='Open a file',
        initialdir='',
        filetypes=filetypes)

    global video_path
    video_path = filename
def select_audio():
    filetypes = (
        ('audio files', '*.mp3'),
        ('All files', '*.*')
    )

    filename = fd.askopenfilename(
        title='Open a file',
        initialdir='',
        filetypes=filetypes)

    global audio_path
    audio_path = filename

def only_numbers(char):
    return char.isdigit()
def run():
    #pedir la ruta del video final 
   
    
    if video_path and audio_path and type_button.get!= '':
        pathout = fd.asksaveasfilename(
        title='Save as',
        initialdir='',
        filetypes=(('mp4 files', '*.mp4'), ('All files', '*.*')))
        if iscut.get()==TRUE:
            if entry_secondsbegin.get()and entry_secondsend.get() != '':
                videosedit(video_path,audio_path,type_button.get(),int(entry_secondsbegin.get()),int(entry_secondsend.get()),pathout=pathout,musicnumber=music_button.get(),level_music=level_music.get())
            else:
                showinfo('Error','Please enter the seconds')
               
        else:
            videosedit(video_path,audio_path,type_button.get(),0,0,pathout=pathout,level_music=level_music.get(),musicnumber=music_button.get())
    else:
        showinfo('Error', 'Please select a file')   


def check():
    if iscut.get()==TRUE:
        entry_secondsbegin.config(state='normal')
        entry_secondsend.config(state='normal')
    else:
        entry_secondsbegin.config(state='disabled')
        entry_secondsend.config(state='disabled')

root = tk.Tk()
root.title('IAcenter editing videos')
root.resizable(False, False)
root.geometry('400x500')
root.configure(background='white')

video_button = Button(
    root,
    text='Open a video',
    command=select_video
)
audio_button = Button(
    root,
    text='Open an audio',
    command=select_audio
)
run_button = Button(
    root,
    text='Run!!!',
    command=run
)


#mostrar un combo box con las opciones de video y audio

type_button = ttk.Combobox(root,state="readonly", values=['Competencias', 'Instrucciones', 'Esperado',"Python","Competencias"])
type_button.current(0)
#crear un checkbox donde habilite los otros 2 botones
validation = root.register(only_numbers)
label_secondsbegin = Label(root, text='Seconds begin:')
label_music = Label(root, text='Music Type:')
label_type = Label(root, text='Type video:')
entry_secondsbegin = Entry(root, validate='key', validatecommand=(validation, '%S'),state="disabled")
label_secondsend = Label(root, text='Seconds end:')
entry_secondsend  =Entry(root,validate='key', state="disabled",validatecommand=(validation, '%S'))
iscut=tk.BooleanVar()
check_button = Checkbutton(root, text='Cut videos???',command=check,variable=iscut)
level_music = tk.Scale(root, from_=5, to=50,resolution=5,orient=tk.HORIZONTAL)
level_music.set(30)
music_button = ttk.Combobox(root,state="readonly", values=['1', '2', '3',"4","5","6"])
music_button.current(0)

if __name__ == '__main__':
    video_path, audio_path,tipo_video = '', '',''
    secounds_begin, secounds_end = 0, 0
    video_button.pack(expand=True)
    audio_button.pack(expand=True)
    type_button.pack(expand=True)
    label_music.pack(expand=True)
    music_button.pack(expand=True)
    level_music.pack(expand=True)  
    run_button.pack(expand=True)
    check_button.pack(expand=True)
    label_secondsbegin.pack(expand=True)
    entry_secondsbegin.pack(expand=True)
    label_secondsend.pack(expand=True)
    entry_secondsend.pack(expand=True)
    tipo_video = type_button.get()
    
    root.mainloop()    





#