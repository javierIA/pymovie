from moviepy.editor import *
from pathlib import Path
from moviepy.video.tools.subtitles import SubtitlesClip
import tkinter as tk
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo




#crear un ventana con 2 label y 3 botones
def select_video():
    global video_path
    video_path = fd.askopenfilename(filetypes=[("Video files", "*.mp4")])
    label_video.config(text=video_path)
    label_video.pack(expand=True)
def select_subtitules():
    global subtitles_path
    subtitles_path = fd.askopenfilename(filetypes=[("Subtitules files", "*.srt")])
    label_subtitules.config(text=subtitles_path)
    label_subtitules.pack(expand=True)
def select_output():
    global output_path
    output_path = fd.asksaveasfilename(filetypes=[("Video files", "*.mp4")])
    label_output.config(text=output_path)
    label_output.pack(expand=True)


def run():
    if video_path !="" or subtitles_path !="" or output_path !="":
        select_output()
        
        subtitules(video_path,subtitles_path,output_path)
    else:
        showinfo('Error', 'Please select a file')   


def subtitules(videopath,subtitlespath,videopathout):
     
    generator = lambda txt: TextClip(txt, font='Pumpkin_Cheesecake', fontsize=100, color='yellow')
    subtitles = SubtitlesClip(subtitlespath, generator)

    video = VideoFileClip(videopath)
    result = CompositeVideoClip([video, subtitles.set_pos(('center','bottom'))],use_bgclip=False)
    result.write_videofile(f"{videopathout}", fps=video.fps, temp_audiofile="tmp-audio.m4a", remove_temp=True, codec="libx264", audio_codec="aac")




#darle un tema con el fondo blanco y el texto negro 
#cambiarle la font a todos los componentes por la fuente arial 12
root = tk.Tk()
font_tuple=("Arial",20,"bold")
root.title('IAcenter editing videos')
#poner el fondo blanco la ventan 
root.configure(background='white')
label_video = tk.Label(root,text='Video')
label_subtitules = tk.Label(root,text='Subtitules')
label_output = tk.Label(root,text='Output')
button_video = tk.Button(root,text='Video',command=select_video)
button_subtitules = tk.Button(root,text='Subtitules',command=select_subtitules)
button_output = tk.Button(root,text='Output',command=select_output)
button_run = tk.Button(root,text='Run',command=run)
label_video.pack(expand=True)
button_video.pack(expand=True)
label_subtitules.pack(expand=True)
button_subtitules.pack(expand=True) 
label_output.pack(expand=True)
button_run.pack(expand=True)
button_run.configure(font=font_tuple)
button_output.configure(font=font_tuple)
label_video.configure(font=font_tuple)
label_output.configure(font=font_tuple)
root.mainloop()


